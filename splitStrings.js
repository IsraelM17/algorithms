// Complete the solution so that it splits the string into pairs of two characters. If the string contains an odd
// number of characters then it should replace the missing second character of the final pair with an
// underscore ('_').

// Examples:
// solution('abc') // should return ['ab', 'c_']
// solution('abcdef') // should return ['ab', 'cd', 'ef']

function solution(str) {
  return str.length > 0
    ? str.match(/.{1,2}/g).map((word) => {
        if (word.length === 1) return (word += "_");
        return word;
      })
    : str.split("");
}

console.log(solution("abcde"));

// best solution
const solution2 = (str) => (str + "_").match(/.{2}/g) || [];

console.log(solution2("abcdefgha_"));
