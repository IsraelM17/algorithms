/** My solution */
function isIsogram(str = "") {
  const arr = str.toUpperCase().split("");
  for (const [index, char] of arr.entries()) {
    for (let i = index + 1; i < arr.length; i++) {
      if (i < arr.length && char === arr[i]) {
        return false;
      }
    }
  }
  return true;

  // return new Set(str.toUpperCase()).size == str.length;

  // return !/(\w).*\1/i.test(str);
}

console.log(isIsogram("mose"));
