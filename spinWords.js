/**
 * Write a function that takes in a string of one or more words, and returns the same string,
 * but with all five or more letter words reversed (like the name of this kata).
 * -> Strings passed in will consist of only letters and spaces.
 * -> Spaces will be included only when more than one word is present.
 *
 * Examples:
 * -> spinWords("Hey fellow warriors") => "Hey wollef sroirraw" 
 * -> spinWords("This is a test") => "This is a test" 
 * -> spinWords("This is another test") => "This is rehtona test"

 */

// My 1st solution
function spinnWords(sentence) {
  const words = sentence.split(" ");
  let reverseSentence = "";
  words.forEach((word) => {
    if (word.length >= 5)
      reverseSentence += word.split("").reverse().join("") + " ";
    else reverseSentence += word + " ";
  });
  return reverseSentence.trimEnd();
}

// This solution is more clear and legible to read
function spinWords2(sentence) {
  return sentence
    .split(" ")
    .map((word) =>
      word.length >= 5 ? word.split("").reverse().join("") : word
    )
    .join(" ");
}

console.log(spinWords2("this is a sentence"));
