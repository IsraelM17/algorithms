const startPerformance = Date();
function factorial(number) {
  let fac = number;
  let counter = number - 1;
  if (number === 0) {
    return number;
  } else {
    while (counter >= 1) {
      fac = fac * counter;
      counter--;
    }
  }
  return fac;
}

const endPerformance = Date();
console.log(
  `Function starts at: ${startPerformance} and finish at ${endPerformance}`
);

function factorialRecursive(number) {
  if (number === 0) {
    return 1;
  }
  return number * factorialRecursive(number - 1);
}

// console.log(factorialRecursive(3));
