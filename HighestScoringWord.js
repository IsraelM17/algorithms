// Given a string of words, you need to find the highest scoring word.
// Each letter of a word scores points according to its position in the alphabet: a = 1, b = 2, c = 3 etc.
// You need to return the highest scoring word as a string.
// If two words score the same, return the word that appears earliest in the original string.
// All letters will be lowercase and all inputs will be valid.

function high(x) {
  let values = {};
  "abcdefghijklmnopqrstuvwxyz".split("").forEach((item, index) => {
    values = { ...values, [item]: index + 1 };
  });

  const words = x.split(" ");

  const pointsByWords = words.map((word) => {
    let points = 0;
    word.split("").forEach((letter) => {
      points += values[letter];
    });
    return points;
  });

  return words[pointsByWords.indexOf(Math.max(...pointsByWords))];
}

function high2(s) {
  const reducer = (a, b) => {
    console.log(a, b);
    // console.log(b.charCodeAt(0));
    return a + b.charCodeAt(0) - 96;
  };

  let as = s.split(" ").map((s) => {
    // console.log([...s]);
    return [...s].reduce(reducer, 0);
  });
  return s.split(" ")[as.indexOf(Math.max(...as))];
}

console.log(high2("abcdj kmnop"));
